#!/bin/zsh

Packages=(

    ## Core
    tmux

    ## emacs
    emacs
    texlive-full
    ripgrep
    fd-find
    hunspell-fr
    hunspell-en-us

    ## CLI
    peco
    tree
    fasd
    bat
    mlocate
    exa
    lftp

    ## Programming
     r gcc-fortran tk
     npm # needed for bash lsp

    ## Web
    hugo go

    ## Fun
    cowsay
    lolcat
    fortune-mod
)

for pkg in "${Packages[@]}"; do
  sudo apt install -y  $pkg
done
